﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json.Linq;

namespace REST_API_PHP
{
    public class InsetrIntoData
    {
        public string Column1 { get; set; }
        
    }
    
    public partial class FilesList : Window
    {
        public static String ImgUrl;
        public FilesList()
        {
            InitializeComponent();
            


            WebClient client = new WebClient();
            var str = client.DownloadString("http://u169832.com4.ru/index.php?getlist=1");

            dynamic jObject = JObject.Parse(str);//json parse

            foreach (var prop in jObject)
            {
                dataGrid.Items.Add(new InsetrIntoData { Column1 = prop.Value });
            }

        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            
            int SelectedItemsCount = dataGrid.SelectedItems.Count;
            for (int i = 0; i < SelectedItemsCount; i++)
            {
                int s = dataGrid.Items.IndexOf(dataGrid.SelectedItems[i]);

                var ci = new DataGridCellInfo(dataGrid.Items[s], dataGrid.Columns[0]);
                var content = ci.Column.GetCellContent(ci.Item) as TextBlock;
                
                String up = content.Text;
                string localFilename = $@"c:\photos\{up}";
                using (WebClient client = new WebClient())
                {
                    client.DownloadFile($"http://u169832.com4.ru/files/{up}", localFilename);
                }

            }
            MessageBox.Show("Успешно загружено!");

        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            int SelectedItemsCount = dataGrid.SelectedItems.Count;
            for (int i = 0; i < SelectedItemsCount; i++)
            {
                int s = dataGrid.Items.IndexOf(dataGrid.SelectedItem);
                
                var ci = new DataGridCellInfo(dataGrid.Items[s], dataGrid.Columns[0]);
                var content = ci.Column.GetCellContent(ci.Item) as TextBlock;
                String up = content.Text;
                WebClient client = new WebClient();
                client.DownloadString($"http://u169832.com4.ru/?delfile=1&file={up}");
                dataGrid.Items.RemoveAt(s);

            }
            MessageBox.Show("ОК");
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            
            
                int s = dataGrid.Items.IndexOf(dataGrid.SelectedItem);

                var ci = new DataGridCellInfo(dataGrid.Items[s], dataGrid.Columns[0]);
                var content = ci.Column.GetCellContent(ci.Item) as TextBlock;
                
                String up = content.Text;
                string localFilename = $@"c:\photos\{up}";
                using (WebClient client = new WebClient())
                {
                    client.DownloadFile($"http://u169832.com4.ru/files/{up}", localFilename);
                }

            ImgUrl = localFilename;
            ShowImg SI = new ShowImg();
            SI.Show();

            
        }
        
    }
}
