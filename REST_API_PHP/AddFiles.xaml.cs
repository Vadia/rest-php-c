﻿using Microsoft.Win32;
using System.IO;
using System.Net;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace REST_API_PHP
{
    
    public partial class AddFiles : Window
    {
        public AddFiles()
        {
            InitializeComponent();
        }
        String dir = "";
        String namesoffiles = "";
        byte[] responseArray;
        private void button_Click(object sender, RoutedEventArgs e)
        {
            WebClient myWebClient = new WebClient();
            OpenFileDialog OPF = new OpenFileDialog();
            OPF.Multiselect = true;
            
            if (OPF.ShowDialog() == true)
            {
                foreach (String file in OPF.FileNames) {
                            namesoffiles += System.IO.Path.GetFileName(OPF.FileName) + ",";

                            responseArray = myWebClient.UploadFile("http://u169832.com4.ru/index.php", $"{file}");
                }
                label.Content = namesoffiles;
                MessageBox.Show(System.Text.Encoding.Default.GetString(responseArray));
            }

        }
    }
}
